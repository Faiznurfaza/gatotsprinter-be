### DEPLOYMENT BACK-END REPOSITORY FOR CH 11

### Whats This Project About ?
Gatot Sprinter is online game web-based project that serves various games that player can play and achievments to get.

Tech used :
- React/Next JS
- Cloudinary
- Tailwind
- Redux
- Bcrypt
- Express JS
- Passport JS
- JWT
- Nodemailer
- PostgreSql Sequelize

Demo (Website) : https://gatotsprinter.vercel.app/
Backend Server : https://gatotsprinter-be.vercel.app/

##### If you want to check the testing back-end repository, please go to https://gitlab.com/bkhaqi/test-c9-be