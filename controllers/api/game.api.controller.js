const db = require('../../models');
const { Sequelize } = require("sequelize");
const Game = db.games

module.exports = {
  getGame: async (req, res) => {
    try {
      const gid = parseInt(req.params.id, 10)
      const game = await Game.findByPk(gid)
      if (!game) {
        return res.status(400).json({
          result: 'ERROR',
          msg: 'Error occured while retrieving data'
        })
      }
      res.status(200).json({
        result: 'Success retrieving data',
        // eslint-disable-next-line object-shorthand
        game: game
      })
    } catch (error) {
      if (error instanceof Sequelize.DatabaseError) {
        throw new Error("Database Error");
      } else {
        res.status(500).json({ error: "Internal server error" });
      }
    }
  },
  getAllGames: async (req, res) => {
    try {
      const games = await Game.findAll()
      res.status(200).json({
        result: 'Success retrieving data',
        // eslint-disable-next-line object-shorthand
        games: games
      })
    } catch (error) {
      if (error instanceof Sequelize.DatabaseError) {
        throw new Error("Database Error");
      } else {
        res.status(500).json({ error: "Internal server error" });
      }
    }
  }
}
