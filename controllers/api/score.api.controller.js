/* eslint-disable object-shorthand */
/* eslint-disable no-unused-vars */
const db = require("../../models");
const { Sequelize } = require("sequelize");
const Score = db.scores;
const User = db.users;
const Game = db.games;

module.exports = {
  addScore: async (req, res) => {
    const userId = req.params.id;
    if(typeof userId !== 'string') return res.status(400).json({
      message: "Bad Request",
      error: "Invalid user ID"
    })
    const gameId = parseInt(req.params.gameId);
    if(isNaN(gameId)) return res.status(400).json({
      message: "Bad Request",
      error: "Invalid game ID"
    })

    try {
      const user = await User.findByPk(userId);
      const game = await Game.findByPk(gameId);
      if (!game) return res.status(404).json({ error: "Game ID not found" });
      if (user) {
        const score = await Score.findOne({
          where: { userId: userId, gameId: gameId },
        });
        if (score) {
          await Score.update(
            { score: score.score + 10 },
            { where: { userId: userId, gameId: gameId } }
          );
          return res.status(200).json({
            result: "SUCCESS",
            msg: "Score updated!",
          });
        } else {
          const data = await Score.create({
            userId: userId,
            gameId: gameId,
            score: 10,
          });
          return res.status(201).json({
            result: "SUCCESS",
            msg: "Score updated!",
            data: {
              id: data.id,
              userId: data.userId,
              gameId: data.gameId,
              score: data.score,
            },
          });
        }
      } else {
        return res.status(404).json({
          result: "ERROR",
          msg: "User not found",
        });
      }
    } catch (error) {
      //console.log(error)
      res.status(500).json({ error: "Internal server error" });
    }
  },
  getScore: async (req, res) => {
    try {
      const userId = req.params.id;
      if(typeof userId !== 'string') return res.status(400).json({
        result: "Bad Request",
        message: "Invalid user ID"
      })
      const user = await User.findByPk(userId);
      if (!user)
        return res.status(404).json({
          result: "Error",
          message: "User ID Not Found",
        });
      const score = await Score.findAll({
        where: {
          userId: userId,
        },
        include: {
          model: User,
          attributes: ["username"],
        },
      });

      res.status(200).json({
        result: "Success",
        score: score,
      });
    } catch (error) {
      if (error instanceof Sequelize.DatabaseError) {
        throw new Error("Database Error");
      } else {
        res.status(500).json({ error: "Internal server error" });
      }
    }
  },
  getAllScore: async (req, res) => {
    try {
      if (Object.keys(req.body).length !== 0) {
        return res.status(403).json({ Error: "Forbidden action" });
      }
      const scores = await Score.findAll({
        include: {
          model: User,
          attributes: ["username"],
        },
      });
      res.status(200).json({
        result: "Success",
        scores: scores,
      });
    } catch (error) {
      if (error instanceof Sequelize.DatabaseError) {
        throw new Error("Database Error");
      } else {
        res.status(500).json({ error: "Internal server error" });
      }
    }
  },
  Leaderboard: async (req, res) => {
    const gameId = req.params.gameId;
    if(!gameId) return res.status(400).json({
      result: "Bad Request",
      message:"Missing Game ID"
    })
    if (isNaN(gameId)) {
      return res.status(400).json({
        result: "Error",
        message: "Invalid Game ID",
      });
    }
    const game = await Game.findByPk(gameId);
    if (!game)
      return res.status(404).json({
        result: "Error",
        message: "Game ID not found",
      });
    await Score.findAll({
      where: { gameId: gameId },
      order: [["score", "DESC"]],
      include: {
        model: User,
        attributes: ["username", "photo"],
      },
    })
      .then((leaderboard) => {
        res.status(200).json({
          message: "User Leaderboard",
          data: leaderboard,
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: "Error occurred while fetching leaderboard"
        });
      });
  },
};
