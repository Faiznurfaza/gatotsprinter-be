/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const db = require('../../models');
const User = db.users
const Video = db.videos
const { Sequelize } = require("sequelize");

module.exports = {
    getVideos: async (req, res) => {
        try {
            const userId = req.params.id
            if(!userId) return res.status(400).json({msg: "Bad Request"})
            const user = await User.findOne({where : {id: userId}})
            if(!user) return res.status(404).json({msg: "User not found"})

            const userVideo = await Video.findAll({where : { userId: userId }})
            if(userVideo.length === 0) return res.status(400).json({msg: "Video From this user not found"})

            res.status(200).json({
                msg: "Success",
                data: userVideo
            })
        } catch (error) {
            return res.status(500).json({
                msg: "Internal Server Error"
            })
        }
    },

    addVideo: async (req, res) => {
        try {
            const userId = req.params.id
            const video = req.body.video
            if(!userId) return res.status(400).json({msg: "Bad Request"})

            const user = await User.findOne({where : {id: userId}})
            if(!user) return res.status(404).json({msg: "User not found"})

            if(!video) return res.status(400).json({msg: "Invalid Video File"})

            const userVideo = await Video.findAll({where : { userId: userId }})
            if(userVideo.length >= 3) return res.status(400).json({msg: "Cant add any more videos"})

            

            const newVideo = await Video.create({
                userId: userId,
                Video_URL: video
            })

            res.status(201).json({
                msg: "Success upload video",
                data: newVideo
            })
        } catch (error) {
            console.log(error)
            return res.status(500).json({
                msg: "Internal Server Error"
            })
        }
    },

    deleteVideo: async(req, res) => {
        try {
            const userId = req.params.id
            const videoId = req.params.videoid

            if(!userId) return res.status(400).json({msg: "Bad Request"})
            if(!videoId) return res.status(400).json({msg: "Bad Request"})

            const user = await User.findOne({where : {id: userId}})
            if(!user) return res.status(404).json({msg: "User not found"})

            const video = await Video.findOne({ where : { id: videoId, userId: userId}})
            if(!video) return res.status(404).json({msg: "Video ID not found"})

            await Video.destroy({ where: { id: videoId, userId: userId }});

            res.status(200).json({
                msg: "Success"
            })
        } catch (error) {
            console.log(error)
            return res.status(500).json({
                msg: "Internal server error"
            })
        }
    }
}

