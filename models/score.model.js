module.exports = (sequelize, DataTypes) => {
  const Score = sequelize.define('Scores', {
    userId: {
      type: DataTypes.STRING
    },
    gameId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    achievement: {
      type: DataTypes.STRING,
      allowNull: true
    }
  })
  return Score
}
