/* eslint-disable no-unused-vars */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable eqeqeq */
module.exports = (sequelize, DataTypes) => {
    const Video = sequelize.define('Videos', {
      userId: {
        type: DataTypes.STRING
      },
      Video_URL: {
        type: DataTypes.STRING,
        allowNull: true
      },
    })
    return Video
  }
  