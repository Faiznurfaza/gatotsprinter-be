/* eslint-disable no-unused-vars */
const bodyParser = require('body-parser')
const videoControllerAPI = require('../../controllers/api/video.api.controller')

const jsonParser = bodyParser.json()
const router = require('express').Router()

router.get('/:id', videoControllerAPI.getVideos)
router.post('/add/:id', videoControllerAPI.addVideo)
router.post('/delete/:id/:videoid', videoControllerAPI.deleteVideo)

module.exports = router