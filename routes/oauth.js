/* eslint-disable */
const passport = require('../config/passport')
const router = require('express').Router()

// Google login route
router.get('/auth/google', (req, res) => {
  const googleAuthUrl = 'https://accounts.google.com/o/oauth2/auth'

  const params = new URLSearchParams({
    response_type: 'code',
    redirect_uri: 'https://gatotsprinter-be.vercel.app/api/v1/user/auth/google/home',
    scope: 'profile email',
    client_id: process.env.CLIENT_ID
  })

  const authUrl = `${googleAuthUrl}?${params}`

  res.send({ authUrl })
})

// Google callback route
router.get(
  '/auth/google/home',
  passport.authenticate('google', {
    failureRedirect: 'https://gatotsprinter.vercel.app/login'
  }),
  (req, res) => {
    const userId = req.user.id // Assuming the user object is available in req.user
    console.log(userId)
    // Redirect to the home page with the userId as a query parameter
    res.redirect(`https://gatotsprinter.vercel.app/home?userId=${userId}`)
  }
)

// Facebook login route
router.get('/auth/facebook', (req, res) => {
  const facebookAuthUrl = 'https://www.facebook.com/v3.2/dialog/oauth'

  const params = new URLSearchParams({
    response_type: 'code',
    redirect_uri:
      'https://gatotsprinter-be.vercel.app/api/v1/user/auth/facebook/home',
    scope: 'email',
    client_id: process.env.FACEBOOK_APP_ID
  })

  const authUrl = `${facebookAuthUrl}?${params}`

  res.send({ authUrl })
})

// Facebook callback route
router.get(
  '/auth/facebook/home',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  (req, res) => {
    const userId = req.user.id
    console.log(userId)
    res.redirect(`https://gatotsprinter.vercel.app/home?userId=${userId}`)
  }
)

// GitHub login route
router.get('/auth/github', (req, res) => {
  const githubAuthUrl = 'https://github.com/login/oauth/authorize'

  const params = new URLSearchParams({
    response_type: 'code',
    redirect_uri: 'https://gatotsprinter-be.vercel.app/api/v1/user/auth/github/home',
    client_id: process.env.GITHUB_CLIENT_ID,
    scope: 'user:email'
  })

  const authUrl = `${githubAuthUrl}?${params}`

  res.set('Access-Control-Allow-Origin', '*') // Allow requests from any origin (replace '*' with your specific origin if needed)
  res.set(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )

  res.send({ authUrl })
})

// GitHub callback route
router.get(
  '/auth/github/home',
  passport.authenticate('github', { failureRedirect: '/login' }),
  (req, res) => {
    const userId = req.user.id
    console.log(userId)
    res.redirect(`https://gatotsprinter.vercel.app/home?userId=${userId}`)
  }
)

// Twitter login route
router.get('/auth/twitter', (req, res) => {
  const twitterAuthUrl = 'https://api.twitter.com/oauth/authenticate'

  const params = new URLSearchParams({
    response_type: 'code',
    redirect_uri: 'https://gatotsprinter-be.vercel.app/api/v1/user/auth/twitter/home',
    scope: 'email',
    client_id: process.env.TWITTER_CONSUMER_KEY
  })

  const authUrl = `${twitterAuthUrl}?${params}`

  res.set('Access-Control-Allow-Origin', '*') // Allow requests from any origin (replace '*' with your specific origin if needed)
  res.set(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )

  res.send({ authUrl })
})

// Twitter callback route
router.get(
  '/auth/twitter/home',
  passport.authenticate('twitter', { failureRedirect: '/login' }),
  (req, res) => {
    const userId = req.user.id
    console.log(userId)
    res.redirect(`https://gatotsprinter.vercel.app/home?userId=${userId}`)
  }
)

module.exports = router
