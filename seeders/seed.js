/* eslint-disable */
const db = require('../models')
const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const Game = db.games
const User = db.users
const Score = db.scores
const Video = db.videos

function updateScore() {
  return [
    db.scores.update(
      { achievement: 'Newcomers' },
      { where: { score: { [Op.lt]: 50 } } }
    ),
    db.scores.update(
      { achievement: 'Bronze Rank' },
      { where: { score: { [Op.between]: [50, 99] } } }
    ),
    db.scores.update(
      { achievement: 'Silver Rank' },
      { where: { score: { [Op.between]: [100, 199] } } }
    ),
    db.scores.update(
      { achievement: 'Gold Rank' },
      { where: { score: { [Op.between]: [200, 299] } } }
    ),
    db.scores.update(
      { achievement: 'Platinum Rank' },
      { where: { score: { [Op.between]: [300, 499] } } }
    ),
    db.scores.update(
      { achievement: 'Diamond Rank' },
      { where: { score: { [Op.between]: [500, 999] } } }
    ),
    db.scores.update(
      { achievement: 'Ruby Rank' },
      { where: { score: { [Op.between]: [1000, 1499] } } }
    ),
    db.scores.update(
      { achievement: 'Legend Rank' },
      { where: { score: { [Op.gt]: 1500 } } }
    )
  ]
}

function SeedGames() {
  db.games.bulkCreate(
    [
      {
        id: 1,
        title: 'Rock Paper Scissor',
        description:
          'The familiar game of Rock, Paper, Scissors is played like this: at the same time, two players display one of three symbols: a rock, paper, or scissors. A rock beats scissors, scissors beat paper by cutting it, and paper beats rock by covering it.',
        thumbnail: 'https://i.ibb.co/8cj8wHx/top-title.webp',
        play_count: 0,
        how_to_play:
          'You have to choose between rock, scissors, or paper. After that you click on your choice and wait to see the results win, lose, or draw',
        release_date: new Date('2023-05-15'),
        latest_update: 'May 15, 2023'
      },
      {
        id: 2,
        title: 'Guess The Number',
        description:
          'The number guessing game is a game where the computer generates a random number and we need to guess that number in minimum tries',
        thumbnail:
          'https://easyshiksha.com/online_courses/assets/games/number-guessing/images/banner.png',
        play_count: 0,
        how_to_play: 'Guess the number from 1 - 100',
        release_date: new Date('2023-05-15'),
        latest_update: 'May 15, 2023'
      },
      {
        id: 3,
        title: 'Tic-Tac-Toe',
        description:
          'Tic-Tac-Toe is a game in which two players seek in alternate turns to complete a row, a column, or a diagonal with either three Os or three Xs drawn in the spaces of a grid of nine squares.',
        thumbnail:
          'https://i.ibb.co/J2PDC73/Screenshot-2023-07-08-034134.png',
        play_count: 0,
        how_to_play:
          'Use the mouse! Right-click/arrow-keys to rotate items while holding them. Get as far into the Dungeon as you can!',
        release_date: new Date('2023-07-07'),
        latest_update: 'July 07, 2023'
      },
      {
        id: 4,
        title: 'Tetris',
        description:
          'The goal of Tetris N-Blox is to score as many points as possible by clearing horizontal rows of Blocks',
        thumbnail:
          'https://play-lh.googleusercontent.com/za2Nu_qjMw5GzWfbzet4zeiZT1xvJlTRi4NJzGpJWX9grxFAAko5dGBwe7qeqK01THw',
        play_count: 0,
        how_to_play:
          'The player must rotate, move, and drop the falling Tetriminos inside the Matrix (playing field). Lines are cleared when they are completely filled with Blocks and have no empty spaces.',
        release_date: new Date('2023-05-17'),
        latest_update: 'May 17, 2023'
      }
    ],
    {
      updateOnDuplicate: [
        'title',
        'description',
        'thumbnail',
        'play_count',
        'how_to_play',
        'release_date',
        'latest_update'
      ]
    }
  )
}

const updatePhotoColumn = async () => {
  try {
    // eslint-disable-next-line no-prototype-builtins
    const hasPhotoColumn = await db.users
      .describe()
      .then(fields => fields.hasOwnProperty('photo'))

    if (!hasPhotoColumn) {
      await db.users.sync({ alter: true })
      //console.log('Added the "photo" column to the User table.')
    }
  } catch (error) {
    //console.error('Failed to update the "photo" column:', error)
  }
}

// Use async/await to ensure the timeouts are completed before Jest finishes
function initializeModels() {
  return Promise.all([
    SeedGames(),
    updateScore(),
    updatePhotoColumn()
  ])
}

initializeModels()
