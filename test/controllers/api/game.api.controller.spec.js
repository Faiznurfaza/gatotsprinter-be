/* eslint-disable */
const base = require('../../../controllers/api/game.api.controller');

const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

const data = [
    {
        "id": 1,
        "title": "Rock Paper Scissor",
        "description": "The familiar game of Rock, Paper, Scissors is played like this: at the same time, two players display one of three symbols: a rock, paper, or scissors. A rock beats scissors, scissors beat paper by cutting it, and paper beats rock by covering it.",
        "thumbnail": "https://i.ibb.co/8cj8wHx/top-title.webp",
        "play_count": 0,
        "how_to_play": "You have to choose between rock, scissors, or paper. After that you click on your choice and wait to see the results win, lose, or draw",
        "release_date": "2023-05-15T00:00:00.000Z",
        "latest_update": "May 15, 2023",
        "createdAt": "2023-06-04T12:03:09.604Z",
        "updatedAt": "2023-06-04T12:03:09.604Z"
    },
    {
        "id": 2,
        "title": "Guess The Number",
        "description": "The number guessing game is a game where the computer generates a random number and we need to guess that number in minimum tries",
        "thumbnail": "https://easyshiksha.com/online_courses/assets/games/number-guessing/images/banner.png",
        "play_count": 0,
        "how_to_play": "Guess the number from 1 - 100",
        "release_date": "2023-05-15T00:00:00.000Z",
        "latest_update": "May 15, 2023",
        "createdAt": "2023-06-04T12:03:09.604Z",
        "updatedAt": "2023-06-04T12:03:09.604Z"
    },
    {
        "id": 3,
        "title": "Backpack Hero",
        "description": "Backpack Hero is the inventory management roguelike! Collect rare items, organize your bag, and vanquish your foes!",
        "thumbnail": "https://cdn.akamai.steamstatic.com/steam/apps/1970580/header.jpg?t=1686321906",
        "play_count": 0,
        "how_to_play": "Use the mouse! Right-click/arrow-keys to rotate items while holding them. Get as far into the Dungeon as you can!",
        "release_date": "2023-05-17T00:00:00.000Z",
        "latest_update": "May 17, 2023",
        "createdAt": "2023-06-24T10:10:50.687Z",
        "updatedAt": "2023-06-24T10:10:50.687Z"
    },
    {
        "id": 4,
        "title": "Tetris",
        "description": "The goal of Tetris N-Blox is to score as many points as possible by clearing horizontal rows of Blocks",
        "thumbnail": "https://play-lh.googleusercontent.com/za2Nu_qjMw5GzWfbzet4zeiZT1xvJlTRi4NJzGpJWX9grxFAAko5dGBwe7qeqK01THw",
        "play_count": 0,
        "how_to_play": "The player must rotate, move, and drop the falling Tetriminos inside the Matrix (playing field). Lines are cleared when they are completely filled with Blocks and have no empty spaces.",
        "release_date": "2023-05-17T00:00:00.000Z",
        "latest_update": "May 17, 2023",
        "createdAt": "2023-06-24T10:10:50.687Z",
        "updatedAt": "2023-06-24T10:10:50.687Z"
    }
]

describe("getGame /game/id/:id", () => {
    test('should fetch game by id with right id', async () => { 
        const req = {
            params: {
                id: 1
            }
        }

        const res = mockResponse();
        await base.getGame(req, res);

        expect(res.status).toBeCalledWith(200)
        expect(res.json).toBeCalledWith({ 
            result: 'Success retrieving data',
            game: expect.objectContaining({
                    id: 1,
                }),
            })
    })

    test('should fetch game by id with right title', async () => { 
        const req = {
            params: {
                id: 1
            }
        }

        const res = mockResponse();
        await base.getGame(req, res);

        expect(res.status).toBeCalledWith(200)
        expect(res.json).toBeCalledWith({ 
            result: 'Success retrieving data',
            game: expect.objectContaining({
                    title: "Rock Paper Scissor",
                }),
            })
    })

    test('should return ERROR id not found', async () => { 
        const req = {
            params: {
                id: 5
            }
        }

        const res = mockResponse();
        await base.getGame(req, res);

        expect(res.status).toBeCalledWith(400)
        expect(res.json).toBeCalledWith({ 
            result: 'ERROR',
            msg: 'Error occured while retrieving data'
        })
    })

    test('should return Internal server error when id isNaN', async () => { 
        const req = {
            params: {
                id: isNaN
            }
        }
        const res = mockResponse();

        const dbError = new Error("Database Error");

        base.getAllGames = jest.fn().mockImplementation(async () => {
            throw dbError;
        });

        await expect(base.getAllGames(req, res)).rejects.toThrow(
            "Database Error"
        );
    })

    test('should return Internal server error', async () => { 
        const req = mockRequest();
        const res = mockResponse();

        base.getGame = jest.fn().mockImplementation(() => {
            res.status(500).json({
                error: "Internal server error",
            });
        });
        await base.getGame(req, res);

        expect(res.status).toBeCalledWith(500)
        expect(res.json).toBeCalledWith({ 
            error: 'Internal server error'
        })
    })
})

describe("getAllGame /game/all", () => {
    test('should return all game data', async () => { 
        const req = mockRequest();
        const res = mockResponse();

        const expectedData = data.map(game => {
            return {
                ...game,
            }
        })

        base.getAllGames = jest.fn().mockImplementation(() => {
            res.status(200).json({
                result: "Success retrieving data",
                games: expectedData,
            });
        });

        await base.getAllGames(req, res);

        expect(res.status).toBeCalledWith(200)
        expect(res.json).toBeCalledWith({
            result: 'Success retrieving data', 
            games: expectedData
        })
     })

    test('should return empty game data', async () => {
        const req = mockRequest();
        const res = mockResponse();

        base.getAllGames = jest.fn().mockImplementation(() => {
            res.status(200).json({
                result: "Success retrieving data",
                games: [],
            });
        });

        await base.getAllGames(req, res);

        expect(res.status).toBeCalledWith(200)
        expect(res.json).toBeCalledWith({
            result: 'Success retrieving data', 
            games: []
        })
     })
     
    test('should return internal server error', async () => {
        const req = mockRequest();
        const res = mockResponse();

        base.getAllGames = jest.fn().mockImplementation((req, res) => {
            res.status(500).json({
                error: "Internal server error",
            });
        });

        await base.getAllGames(req, res);

        expect(res.status).toBeCalledWith(500);
        expect(res.json).toBeCalledWith({
            error: "Internal server error",
        });
    })
    
    test("should handle database error", async () => {
        const req = mockRequest();
        const res = mockResponse();

        const dbError = new Error("Database Error");

        base.getAllGames = jest.fn().mockImplementation(async () => {
            throw dbError;
        });

        await expect(base.getAllGames(req, res)).rejects.toThrow(
            "Database Error"
        );
    });

})