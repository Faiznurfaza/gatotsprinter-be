/* eslint-disable */
/* eslint-disable no-unused-vars */
const scoreController = require("../../../controllers/api/score.api.controller");
const { v4: uuidv4 } = require("uuid");
const { Sequelize } = require("sequelize");
const db = require("../../../models");
const Score = db.scores;
const User = db.users;
const Game = db.games;

const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("GET /getAllScore behavior", () => {
  const originalFunction = scoreController.getAllScore;
  test("should fetch all scores data when requested", async () => {
    const req = mockRequest();
    const res = mockResponse();

    await scoreController.getAllScore(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      result: "Success",
      scores: expect.arrayContaining([
        expect.objectContaining({
          userId: expect.any(String),
          gameId: expect.any(Number),
          //achievement: expect.any(String),
          score: expect.any(Number),
          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),
          User: expect.objectContaining({
            username: expect.any(String),
          }),
        }),
      ]),
    });
  });

  test("should handle empty scores data when theres no data", async () => {
    const req = mockRequest();
    const res = mockResponse();

    await scoreController.getAllScore(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      result: "Success",
      scores: expect.any(Array),
    });
  });

  test("should handle when internal server error happens", async () => {
    const req = mockRequest();
    const res = mockResponse();

    scoreController.getAllScore = jest.fn().mockImplementation((req, res) => {
      res.status(500).json({
        error: "Internal server error",
      });
    });

    await scoreController.getAllScore(req, res);
    expect(res.status).toBeCalledWith(500);
    expect(res.json).toBeCalledWith({
      error: "Internal server error",
    });
  });

  test("should handle when database error happens", async () => {
    const req = mockRequest();
    const res = mockResponse();

    const dbError = new Error("Database Error");

    scoreController.getAllScore = jest.fn().mockImplementation(async () => {
      throw dbError;
    });

    await expect(scoreController.getAllScore(req, res)).rejects.toThrow(
      "Database Error"
    );
    scoreController.getAllScore = originalFunction;
  });

  test("if user fill the req.body it should return error 403 status code", async () => {
    const req = mockRequest({ body: {} });
    const res = mockResponse();

    await scoreController.getAllScore(req, res);

    expect(res.status).toBeCalledWith(403);
    expect(res.json).toBeCalledWith({
      Error: "Forbidden action",
    });
  });
  scoreController.getAllScore = originalFunction;
});

// Implementation addScore
describe("POST /addScore/:id/:gameId behavior", () => {
  let originalFunction = scoreController.addScore;
  let gameId;

  beforeAll(async () => {
    userId = "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9";
    gameId = 2;
  });

  afterAll(async () => {
    await Score.destroy({ where: { userId, gameId } });
  });

  test("should handle add score to existing user with existing game id", async () => {
    const req = {
      params: {
        id: "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        gameId: 1,
      },
    };
    const res = mockResponse();

    await scoreController.addScore(req, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      result: "SUCCESS",
      msg: "Score updated!",
    });
  });

  test("should handle create new score to existing user", async () => {
    const req = {
      params: {
        id: userId,
        gameId: gameId,
      },
    };
    const res = mockResponse();
    await scoreController.addScore(req, res);
    expect(res.status).toBeCalledWith(201);
    expect(res.json).toBeCalledWith({
      result: "SUCCESS",
      msg: "Score updated!",
      data: expect.objectContaining({
        id: expect.any(Number),
        userId: userId,
        gameId: gameId,
        score: expect.any(Number),
      }),
    });
    scoreController.addScore = originalFunction;
  });

  test("should handle error when add score to existing user but the game id is not found", async () => {
    const req = {
      params: {
        id: "dd662956-5cc3-4723-95ad-018ed38fe144",
        gameId: 99,
      },
    };
    const res = mockResponse();
    await scoreController.addScore(req, res);
    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      error: "Game ID not found",
    });
  });
  test("should handle error when add score to non-exist user ID", async () => {
    const req = {
      params: {
        id: uuidv4(),
        gameId: 1,
      },
    };
    const res = mockResponse();

    await scoreController.addScore(req, res);
    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      result: "ERROR",
      msg: "User not found",
    });
  });
  test("should handle error when add score to invalid type of user ID", async () => {
    const req = {
      params: {
        id: 1,
        gameId: 1,
      },
    };
    const res = mockResponse();

    await scoreController.addScore(req, res);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      message: "Bad Request",
      error: "Invalid user ID",
    });
  });
});

// Implementation getScore
describe("GET /getScore function behavior", () => {
  const originalFunction = scoreController.getScore;
  test("it should fetch existing score data from existing user", async () => {
    const req = {
      params: {
        id: "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
      },
    };
    const res = mockResponse();
    await scoreController.getScore(req, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      result: "Success",
      score: expect.arrayContaining([
        expect.objectContaining({
          userId: "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        }),
      ]),
    });
  });

  test("it should fetch empty score data from existing user", async () => {
    const req = {
      params: {
        id: "b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b2",
      },
    };
    const res = mockResponse();
    await scoreController.getScore(req, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      result: "Success",
      score: [],
    });
  });

  test("it should returns an error if the requested user ID not found", async () => {
    const req = {
      params: {
        id: uuidv4(),
      },
    };
    const res = mockResponse();
    await scoreController.getScore(req, res);
    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      result: "Error",
      message: "User ID Not Found",
    });
  });

  test("it should returns an error if user id is invalid", async () => {
    const req = {
      params: {
        id: 1,
      },
    };
    const res = mockResponse();
    await scoreController.getScore(req, res);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      result: "Bad Request",
      message: "Invalid user ID",
    });
  });

  test("it should returns an error if database error happens", async () => {
    const req = mockRequest();
    const res = mockResponse();
    const dbError = new Error("Database Error");

    scoreController.getScore = jest.fn().mockImplementation(async () => {
      throw dbError;
    });
    await expect(scoreController.getScore(req, res)).rejects.toThrow(
      "Database Error"
    );
    scoreController.getScore = originalFunction;
  });
});

// Implementation Leaderboard testing
describe("testing GET /leaderboard function behavior", () => {
  const originalFunction = scoreController.Leaderboard;
  test("it should fetch all scores data from gameId", async () => {
    const req = {
      params: {
        gameId: 1,
      },
    };
    const res = mockResponse();

    await scoreController.Leaderboard(req, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User Leaderboard",
      data: expect.arrayContaining([
        expect.objectContaining({
          gameId: 1,
        }),
      ]),
    });
  });

  test("it should fetch all scores data from gameId but the data is an empty array", async () => {
    const req = {
      params: {
        gameId: 1,
      },
    };
    const res = mockResponse();

    scoreController.Leaderboard = jest.fn().mockImplementation((req, res) => {
      res.status(200).json({
        message: "User Leaderboard",
        data: [],
      });
    });

    await scoreController.Leaderboard(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User Leaderboard",
      data: [],
    });
    scoreController.Leaderboard = originalFunction;
  });

  test("if the game ID not found, it will returns an error", async () => {
    const req = {
      params: {
        gameId: 99,
      },
    };
    const res = mockResponse();

    await scoreController.Leaderboard(req, res);
    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      result: "Error",
      message: "Game ID not found",
    });
  });

  test("to test if the game ID requested is invalid type / undefined", async () => {
    const req = {
      params: {
        gameId: "a",
      },
    };
    const res = mockResponse();

    await scoreController.Leaderboard(req, res);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      result: "Error",
      message: "Invalid Game ID",
    });
  });

  test("to test if request with missing gameId parameter", async () => {
    const req = {
      params: "",
    };
    const res = mockResponse();

    await scoreController.Leaderboard(req, res);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      result: "Bad Request",
      message: "Missing Game ID",
    });
  });
});
