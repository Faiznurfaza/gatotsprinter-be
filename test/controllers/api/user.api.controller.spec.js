/* eslint-disable */
require('dotenv').config()
const base = require('../../../controllers/api/user.api.controller')
const db = require('../../../models')
const User = db.users
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

let resetToken
describe('POST /api/v1/user/register', () => {
  test('should return 201 and success message when registration is successful (User1)', async () => {
    const req = {
      body: {
        username: 'user1234',
        email: 'user1234@gmail.com',
        password: 'user1234'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    await base.register(req, res)

    expect(res.status).toBeCalledWith(201)
    expect(res.json).toBeCalledWith({
      msg: 'Registrasi Berhasil',
      data: expect.any(Object)
    })
  })

  test('should return 201 and success message when registration is successful (User2)', async () => {
    const req = {
      body: {
        username: 'user5678',
        email: 'user5678@gmail.com',
        password: 'user5678'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    await base.register(req, res)

    expect(res.status).toBeCalledWith(201)
    expect(res.json).toBeCalledWith({
      msg: 'Registrasi Berhasil',
      data: expect.any(Object)
    })
  })

  test('should return 409 and error message if username is already taken', async () => {
    const req = {
      body: {
        username: 'user5678',
        email: 'user0000@gmail.com',
        password: 'user5678'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    await base.register(req, res)

    expect(res.status).toBeCalledWith(409)
    expect(res.json).toBeCalledWith({
      msg: 'Username telah terpakai'
    })
  })
  test('should return 409 and error message if email is already taken', async () => {
    const req = {
      body: {
        username: 'user0000',
        email: 'user5678@gmail.com',
        password: 'user5678'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    await base.register(req, res)

    expect(res.status).toBeCalledWith(409)
    expect(res.json).toBeCalledWith({
      msg: 'Email telah terpakai'
    })
  })

  test('should return 500 and error message if an error occurs during registration', async () => {
    const req = {
      body: {
        username: 'testuser',
        email: 'test@example.com',
        password: 'password123'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    jest
      .spyOn(User, 'findOne')
      .mockRejectedValueOnce(new Error('Database error'))

    await base.register(req, res)

    expect(res.status).toBeCalledWith(500)
    expect(res.json).toBeCalledWith({ error: 'Internal server error' })
  })
})

describe('POST /api/v1/user/login', () => {
  afterAll(async () => {
    // Delete the test users
    await User.destroy({ where: { username: ['user1234', 'user5678'] } })
  })
  afterEach(() => {
    User.findOne.mockRestore()
    jest.clearAllMocks()
  })

  test('should return 200 and success message when login is successful (User 1)', async () => {
    const req = {
      body: {
        email: 'user1234@gmail.com',
        password: 'user1234'
      },
      session: {}
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      email: req.body.email,
      password: 'hashedPassword',
      id: 'userId'
    }

    User.findOne = jest.fn().mockResolvedValueOnce(user)

    bcrypt.compare = jest.fn().mockResolvedValueOnce(true)

    await base.login(req, res)

    expect(User.findOne).toHaveBeenCalledTimes(1)
    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: req.body.email }
    })

    expect(bcrypt.compare).toHaveBeenCalledTimes(1)
    expect(bcrypt.compare).toHaveBeenCalledWith(
      req.body.password,
      user.password
    )

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      msg: 'Login berhasil',
      accessToken: expect.any(String),
      userId: expect.any(String)
    })
  })
  test('should return 200 and success message when login is successful (User 2)', async () => {
    const req = {
      body: {
        email: 'user5678@gmail.com',
        password: 'user5678'
      },
      session: {}
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      email: req.body.email,
      password: 'hashedPassword',
      id: 'userId'
    }

    User.findOne = jest.fn().mockResolvedValueOnce(user)

    bcrypt.compare = jest.fn().mockResolvedValueOnce(true)

    await base.login(req, res)

    expect(User.findOne).toHaveBeenCalledTimes(1)
    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: req.body.email }
    })

    expect(bcrypt.compare).toHaveBeenCalledTimes(1)
    expect(bcrypt.compare).toHaveBeenCalledWith(
      req.body.password,
      user.password
    )

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      msg: 'Login berhasil',
      accessToken: expect.any(String),
      userId: expect.any(String)
    })
  })

  test('should return 404 and error message if email is not found', async () => {
    const req = {
      body: {
        email: 'nonexistent@gmail.com',
        password: 'user1234'
      },
      session: {}
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    User.findOne = jest.fn().mockResolvedValueOnce(null)

    await base.login(req, res)

    expect(User.findOne).toHaveBeenCalledTimes(1)
    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: req.body.email }
    })

    expect(res.status).toBeCalledWith(404)
    expect(res.json).toBeCalledWith({
      msg: 'Email Tidak Ditemukan'
    })
  })
  test('should return 400 and error message if password is incorrect', async () => {
    const req = {
      body: {
        email: 'user1234@gmail.com',
        password: 'incorrectPassword'
      },
      session: {}
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      email: req.body.email,
      password: 'hashedPassword',
      id: 'userId'
    }

    User.findOne = jest.fn().mockResolvedValueOnce(user)

    bcrypt.compare = jest.fn().mockResolvedValueOnce(false)

    await base.login(req, res)

    expect(User.findOne).toHaveBeenCalledTimes(1)
    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: req.body.email }
    })

    expect(bcrypt.compare).toHaveBeenCalledTimes(1)
    expect(bcrypt.compare).toHaveBeenCalledWith(
      req.body.password,
      user.password
    )

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Password Salah'
    })
  })
  test('should return 500 and error message if an error occurs during login', async () => {
    const req = {
      body: {
        email: 12345,
        password: 12345
      },
      session: {}
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    User.findOne = jest.fn().mockRejectedValueOnce(new Error('Database error'))

    await base.login(req, res)

    expect(User.findOne).toHaveBeenCalledTimes(1)
    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: req.body.email }
    })

    expect(res.status).toBeCalledWith(500)
    expect(res.json).toBeCalledWith({
      error: 'Internal server error'
    })
  })
})
describe('PUT /api/v1/user/edit/:id', () => {
  let originalFindOne
  beforeEach(() => {
    originalFindOne = jest.spyOn(User, 'findOne')
  })
  afterEach(() => {
    originalFindOne.mockRestore()
    jest.clearAllMocks()
  })
  test('should return 200 and success message when username is updated successfully', async () => {
    const req = {
      params: { id: '1026aed7-14cd-44f7-8c05-efa69020db81' },
      body: { username: 'usertest1' }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      id: '1026aed7-14cd-44f7-8c05-efa69020db81',
      username: 'usertest1',
      email: 'usertest@gmail.com',
      password: 'usertest',
      save: jest.fn()
    }

    originalFindOne.mockResolvedValue(user)

    await base.updateProfile(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({ msg: 'Profile updated successfully' })
  })
  test('should return 200 and success message when email is updated successfully', async () => {
    const req = {
      params: { id: '1026aed7-14cd-44f7-8c05-efa69020db81' },
      body: { email: 'usertest1@gmail.com' }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      id: '1026aed7-14cd-44f7-8c05-efa69020db81',
      username: 'usertest',
      email: 'usertest1@gmail.com',
      password: 'usertest',
      save: jest.fn()
    }

    originalFindOne.mockResolvedValue(user)

    await base.updateProfile(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({ msg: 'Profile updated successfully' })
  })

  test('should return 409 and error message if username is already in use', async () => {
    const req = {
      params: { id: '1026aed7-14cd-44f7-8c05-efa69020db81' },
      body: { username: 'usertest1' }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      id: '1026aed7-14cd-44f7-8c05-efa69020db81',
      username: 'usertest1',
      email: 'usertest@gmail.com',
      password: 'usertest',
      save: jest.fn()
    }

    originalFindOne.mockResolvedValue(user.username)

    await base.updateProfile(req, res)

    expect(res.status).toBeCalledWith(409)
    expect(res.json).toBeCalledWith({ msg: 'Username already in use' })
  })
  test('should return 409 and error message if email is already in use', async () => {
    const req = {
      params: { id: '1026aed7-14cd-44f7-8c05-efa69020db81' },
      body: { email: 'usertest@gmail.com' }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      id: '1026aed7-14cd-44f7-8c05-efa69020db81',
      username: 'usertest1',
      email: 'usertest@gmail.com',
      password: 'usertest',
      save: jest.fn()
    }

    originalFindOne.mockResolvedValue(user.email)

    await base.updateProfile(req, res)

    expect(res.status).toBeCalledWith(409)
    expect(res.json).toBeCalledWith({ msg: 'Email already in use' })
  })
  test('should return 500 and error message if if an error occurs during update profile', async () => {
    const req = {
      params: { id: '1026aed7-14cd-44f7-8c05-efa69020db81' }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const user = {
      id: '1026aed7-14cd-44f7-8c05-efa69020db81',
      username: 'usertest1',
      email: 'usertest@gmail.com',
      password: 'usertest',
      save: jest.fn()
    }

    originalFindOne.mockResolvedValue(user.email)

    await base.updateProfile(req, res)

    expect(res.status).toBeCalledWith(500)
    expect(res.json).toBeCalledWith({ error: 'Internal server error' })
  })
})

describe('getOneUser /user/:id', () => {
  beforeAll(() => {
    jest.restoreAllMocks()
  })
  test('should return user data by id with right id', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }
    await base.userData(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      result: 'Success',
      data: expect.any(Object)
    })
  })

  test('should return user data by id with right email', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }
    await base.userData(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      result: 'Success',
      data: expect.objectContaining({
        email: 'unittesting@testing.com'
      })
    })
  })

  test('should return ERROR id not found', async () => {
    const req = {
      params: {
        id: '123123'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }
    await base.userData(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      result: 'ERROR',
      msg: 'Error occured while retrieving data'
    })
  })

  test('should return Internal server error', async () => {
    const req = {
      params: {
        id: '323baa79-4db1-49a2-bfac-7109ea3f322f'
      }
    }

    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    base.userData = jest.fn().mockImplementation((req, res) => {
      res.status(500).json({
        error: 'Internal server error'
      })
    })

    await base.userData(req, res)

    expect(res.status).toBeCalledWith(500)
    expect(res.json).toBeCalledWith({
      error: 'Internal server error'
    })
  })

  test('should handle database error', async () => {
    const req = {}
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis()
    }

    const dbError = new Error('Database Error')

    base.userData = jest.fn().mockImplementation(async () => {
      throw dbError
    })

    await expect(base.userData(req, res)).rejects.toThrow('Database Error')
  })
})

// Mas Haqi
const mockRequest = (body = {}) => ({ body })
const mockResponse = () => {
  const res = {}
  res.json = jest.fn().mockReturnValue(res)
  res.status = jest.fn().mockReturnValue(res)
  return res
}

// Test for forgotPassword
describe('forgotPassword', () => {
  let originalFunction = base.forgotPassword
  afterEach(() => {
    jest.restoreAllMocks()
    base.forgotPassword = originalFunction
  })
  // Positive tests
  test('should send reset password email for a valid email', async () => {
    const req = {
      body: {
        email: 'binarchallenge9@gmail.com'
      }
    }
    const res = mockResponse()

    await base.forgotPassword(req, res)

    expect(res.status).toHaveBeenCalledWith(200)
    expect(res.json).toHaveBeenCalledWith({
      msg: 'Email reset password telah dikirim',
      resetToken: expect.any(String)
    })
    resetToken = res.json.mock.calls[0][0].resetToken
  })

  test('should handle errors when sending reset password email', async () => {
    const req = {
      body: {
        email: 'binarchallenge9@gmail.com'
      }
    }
    const res = mockResponse()

    base.forgotPassword = jest.fn().mockImplementation((req, res) => {
      res.status(500).json({
        error: 'Internal server error'
      })
    })
    await base.forgotPassword(req, res)

    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' })
  })

  // Negative tests
  test('should return an error if email is not found', async () => {
    const req = {
      body: {
        email: 'nonexistent@example.com'
      }
    }
    const res = mockResponse()

    await base.forgotPassword(req, res)

    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.json).toHaveBeenCalledWith({
      msg: 'Email tidak ditemukan',
      email: expect.any(String)
    })
  })

  test('should handle errors when finding user', async () => {
    const req = {
      body: {
        email: 'unittesting@testing.com'
      }
    }
    const res = mockResponse()

    User.findOne = jest.fn().mockRejectedValueOnce(new Error('Database error'))

    await base.forgotPassword(req, res)

    expect(User.findOne).toHaveBeenCalledWith({
      where: { email: 'unittesting@testing.com' }
    })
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' })
  })

  test('should handle errors when catching exceptions', async () => {
    const req = {
      body: {
        email: 'unittesting@testing.com'
      }
    }
    const res = mockResponse()

    base.forgotPassword = jest.fn().mockImplementation((req, res) => {
      res.status(500).json({ error: 'Internal server error' })
    })
    await base.forgotPassword(req, res)

    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' })
  })
})

// Test for resetPassword
describe('resetPassword', () => {
  let originalFunction = base.resetPassword
  afterEach(() => {
    jest.restoreAllMocks()
    base.resetPassword = originalFunction
  })
  // 2 Positive tests
  test('should reset the password and return success message', async () => {
    // Mock request and response objects
    const req = {
      body: {
        resetToken: resetToken,
        password: 'newpassword123'
      }
    }
    const res = mockResponse()

    // Mock User.findOne to return a user
    User.findOne = jest.fn().mockResolvedValueOnce({
      id: '3e34bfd3-85ee-4f58-b5e2-d289b7622a11',
      email: 'binarchallenge9@gmail.com',
      save: jest.fn().mockResolvedValueOnce()
    })

    // Call the controller function
    await base.resetPassword(req, res)

    // Assertion
    expect(res.status).toHaveBeenCalledWith(200)
    expect(res.json).toHaveBeenCalledWith({ msg: 'Reset password berhasil' })
  })

  test('should return an error if the reset token is invalid', async () => {
    // Mock request and response objects
    const req = {
      body: {
        resetToken: 'stringkosong',
        password: 'newpassword123'
      }
    }
    const res = mockResponse()

    // Call the controller function
    await base.resetPassword(req, res)

    // Assertion
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({
      error: 'Internal server error'
    })
  })

  // 3 Negative tests
  test('should return an error if the user is not found', async () => {
    // Mock request and response objects
    const req = {
      body: {
        resetToken: resetToken,
        password: 'testing12345'
      }
    }
    const res = mockResponse()

    // Mock User.findOne to return null
    User.findOne = jest.fn().mockResolvedValueOnce(null)

    // Call the controller function
    await base.resetPassword(req, res)

    // Assertion
    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.json).toHaveBeenCalledWith({ msg: 'User tidak ditemukan' })
  })

  test('should return an error if there is an internal server error', async () => {
    // Mock request and response objects
    const req = {
      body: {
        resetToken: resetToken,
        password: '12345678'
      }
    }
    const res = mockResponse()

    // Mock User.findOne to throw an error
    User.findOne = jest.fn().mockRejectedValueOnce(new Error('Database error'))

    // Call the controller function
    await base.resetPassword(req, res)

    // Assertion
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' })
  })

  test('should return an error if bcrypt hash fails', async () => {
    // Mock request and response objects
    const req = mockRequest({
      body: {
        resetToken: resetToken,
        password: 'testing123'
      }
    })
    const res = mockResponse()

    // Mock User.findOne to return a user
    User.findOne = jest.fn().mockResolvedValueOnce({
      id: '3e34bfd3-85ee-4f58-b5e2-d289b7622a11',
      email: 'binarchallenge9@gmail.com',
      save: jest.fn().mockResolvedValueOnce()
    })

    // Mock bcrypt.hash to throw an error
    bcrypt.hash = jest.fn().mockRejectedValueOnce(new Error('Hashing error'))

    // Call the controller function
    await base.resetPassword(req, res)

    // Assertion
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' })
  })
})

// getUser
describe('getUser', () => {
  let originalFunction = base.getUser
  afterEach(() => {
    jest.restoreAllMocks()
    base.getUser = originalFunction
  })
  test('should return users successfully', async () => {
    // Create mock request and response objects
    const req = mockRequest()
    const res = mockResponse()

    // Call the getUser controller function
    await base.getUser(req, res)

    // Assert that the response contains the expected users
    expect(res.json).toHaveBeenCalledWith({
      data: expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          email: expect.any(String),
          username: expect.any(String)
        })
      ])
    })
  })

  test('should handle empty user list', async () => {
    // Mock the User model's findAll method to return an empty array
    const mockUsers = [{}]
    User.findAll = jest.fn().mockResolvedValue(mockUsers)

    // Create mock request and response objects
    const req = mockRequest()
    const res = mockResponse()

    // Call the getUser controller function
    await base.getUser(req, res)

    // Assert that the response contains the expected users
    expect(res.json).toHaveBeenCalledWith({
      data: expect.any(Array)
    })
  })

  test('should handle error while retrieving users', async () => {
    const dbError = Error('Database error')

    const req = mockRequest()
    const res = mockResponse()

    base.getUser = jest.fn().mockImplementation(async () => {
      throw dbError
    })

    await expect(base.getUser(req, res)).rejects.toThrow('Database error')
  })

  test('should handle error with status code', async () => {
    const dbError = Error('Database error')

    const req = mockRequest()
    const res = mockResponse()

    base.getUser = jest.fn().mockImplementation(async () => {
      throw dbError
    })

    await expect(base.getUser(req, res)).rejects.toThrow('Database error')
  })

  test('should handle error', async () => {
    const dbError = Error('Database error')

    const req = mockRequest()
    const res = mockResponse()

    base.getUser = jest.fn().mockImplementation(async () => {
      throw dbError
    })

    await expect(base.getUser(req, res)).rejects.toThrow('Database error')
  })
})

//UpdatePhoto
describe('updatePhoto', () => {
  let originalFunction = base.updatePhoto
  afterEach(() => {
    jest.restoreAllMocks()
    base.updatePhoto = originalFunction
  })
  let req, res;

  beforeEach(() => {
    req = mockRequest();
    res = mockResponse();
  });

  test('should update the user photo and return status 200 on successful update', async () => {
    // Arrange
    const userId = '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9';
    const photo = 'new-photo-url';
    req.params = { id: userId };
    req.body = { photo };
    User.findOne = jest.fn().mockResolvedValueOnce({ id: userId, save: jest.fn() });

    // Act
    await base.updatePhoto(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Update photo profile success' });
  });

  test('should update the user photo and return status 200 on successful update', async () => {
    // Arrange
    const userId = '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9';
    const photo = 'new-photo-url2';
    req.params = { id: userId };
    req.body = { photo };
    const user = { id: userId, save: jest.fn() };
    User.findOne = jest.fn().mockResolvedValueOnce(user);
  
    // Act
    await base.updatePhoto(req, res);
  
    // Assert
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Update photo profile success' });
  });
  

  test('should return status 404 with error message if user is not found', async () => {
    // Arrange
    const userId = '1c7672a3-bdb2-44ac-acaa-9da2fd4436d';
    const photo = 'new-photo-url';
    req.params = { id: userId };
    req.body = { photo };
    User.findOne = jest.fn().mockResolvedValueOnce(null);

    // Act
    await base.updatePhoto(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({ msg: 'User not found' });
  });

  test('should return status 409 with error message if photo is invalid', async () => {
    // Arrange
    const userId = '3e34bfd3-85ee-4f58-b5e2-d289b7622a11';
    const photo = null;
    req = {
      params: {
        id: userId
      },
      body: {
        photo: photo
      }
    }

    const user = { id: userId, save: jest.fn() };
    User.findOne = jest.fn().mockResolvedValueOnce(user);
    // Act
    await base.updatePhoto(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(409);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Invalid Image' });
  });

  test('should log error while saving the user', async () => {
    // Arrange
    const userId = '1c7672a3-bdb2-44ac-acaa-9da2fd4436';
    const photo = 'new-photo-url';
    // Act
    await base.updatePhoto(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' });
  });
});

// Test for Logout
describe('logout', () => {
  let req, res;

  beforeEach(() => {
    req = mockRequest();
    res = mockResponse();
  });

  afterEach(() => {
    jest.restoreAllMocks()
  });

  test('should destroy the session and return status 200 on successful logout', async () => {
    // Create mock request and response objects
    const req = mockRequest();
    const res = mockResponse();

    // Create a mock session object with a destroy method
    const session = { destroy: jest.fn() };

    // Assign the session object to the request
    req.session = session;

    // Call the logout controller function
    await base.logout(req, res);

    // Assert that the session destroy method is called
    expect(session.destroy).toHaveBeenCalled();

    // Assert that the response contains the expected message
    expect(res.json).toHaveBeenCalledWith({ msg: 'Logout berhasil' });
  });
  
  test('should log error and return status 500 on internal server error while destroying the session', async () => {
    // Arrange Session
    req.session = { userId: '123' };

    console.error = jest.fn();

    // Act
    await base.logout(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' });
  });

  test('should return status 500 with error message if an error occurs while destroying the session', async () => {
    // Arrange Session
    req.session = { userId: '123' };

    // Act
    await base.logout(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' });
  });

  test('should destroy the session and return status 200 on successful logout', async () => {
    // Create mock request and response objects
    const req = mockRequest();
    const res = mockResponse();

    // Create a mock session object with a destroy method
    const session = { destroy: jest.fn() };

    // Assign the session object to the request
    req.session = session;

    // Call the logout controller function
    await base.logout(req, res);

    // Assert that the session destroy method is called
    expect(session.destroy).toHaveBeenCalled();

    // Assert that the response contains the expected message
    expect(res.json).toHaveBeenCalledWith({ msg: 'Logout berhasil' });
  });

  test('should log error and return status 500 on internal server error while destroying the session', async () => {
    // Arrange Session
    req.session = { userId: '123' };
    console.error = jest.fn();

    // Act
    await base.logout(req, res);

    // Assert
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' });
  });
});

