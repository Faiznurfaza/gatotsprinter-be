/* eslint-disable */
/* eslint-disable no-unused-vars */
const videoController = require('../../../controllers/api/video.api.controller')
const { v4: uuidv4 } = require('uuid')
const { Sequelize } = require('sequelize')
const db = require('../../../models')
const User = db.users
const Video = db.videos

const mockRequest = (body = {}) => ({ body })
const mockResponse = () => {
  const res = {}
  res.json = jest.fn().mockReturnValue(res)
  res.status = jest.fn().mockReturnValue(res)
  return res
}

describe('GET /getVideos behavior', () => {
  const originalFunction = videoController.getVideos
  afterEach(() => {
    videoController.getVideos = originalFunction
  })
  test('Should fetch all videos data from user when requested', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9'
      }
    }
    const res = mockResponse()
    await videoController.getVideos(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      msg: 'Success',
      data: expect.any(Array)
    })
  })

  test('Should handle empty videos data when theres no data', async () => {
    const req = {
      params: {
        id: 'd4c8daa2-c443-402b-ae62-7501d7917dfa'
      }
    }
    const res = mockResponse()
    await videoController.getVideos(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Video From this user not found'
    })
  })

  test('Should handle 404 error when user id not found', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd443639'
      }
    }
    const res = mockResponse()
    await videoController.getVideos(req, res)

    expect(res.status).toBeCalledWith(404)
    expect(res.json).toBeCalledWith({
      msg: 'User not found'
    })
  })

  test('Should handle error when req.params.id not presence', async () => {
    const req = {
      params: {
        id: ''
      }
    }
    const res = mockResponse()

    await videoController.getVideos(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Bad Request'
    })
  })

  test('Should handle error when internal server error happens', async () => {
    const req = mockRequest()
    const res = mockResponse()

    await videoController.getVideos(req, res)

    expect(res.status).toBeCalledWith(500)
    expect(res.json).toBeCalledWith({
      msg: 'Internal Server Error'
    })
  })
})

describe('POST /addVideo behavior', () => {
  let originalFunction = videoController.addVideo
  let newVideoId
  afterEach(async () => {
    videoController.addVideo = originalFunction
    if (newVideoId) {
      await Video.destroy({ where: { id: newVideoId } })
    }
  })
  test('Should return success when addvideo success', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9'
      },
      body: {
        video:
          'https://res.cloudinary.com/dqvhvly7q/video/upload/v1688334862/profiles/moxogt6rruhib0jmmjhc.mp4'
      }
    }
    const res = mockResponse()

    await videoController.addVideo(req, res)
    newVideoId = res.json.mock.calls[0][0].data.id
    expect(res.status).toBeCalledWith(201)
    expect(res.json).toBeCalledWith({
      msg: 'Success upload video',
      data: expect.any(Object)
    })
  })

  test('Should return error when theres no req.params.id', async () => {
    const req = {
      params: {
        id: ''
      },
      body: {
        video:
          'https://res.cloudinary.com/dqvhvly7q/video/upload/v1688334862/profiles/moxogt6rruhib0jmmjhc.mp4'
      }
    }

    const res = mockResponse()

    await videoController.addVideo(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Bad Request'
    })
  })

  test('Should return error when user not found', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d1'
      },
      body: {
        video:
          'https://res.cloudinary.com/dqvhvly7q/video/upload/v1688334862/profiles/moxogt6rruhib0jmmjhc.mp4'
      }
    }

    const res = mockResponse()

    await videoController.addVideo(req, res)
    expect(res.status).toBeCalledWith(404)
    expect(res.json).toBeCalledWith({
      msg: 'User not found'
    })
  })

  test('Should return error when user has 3 videos', async () => {
    const req = {
      params: {
        id: 'b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b2'
      },
      body: {
        video:
          'https://res.cloudinary.com/dqvhvly7q/video/upload/v1688334862/profiles/moxogt6rruhib0jmmjhc.mp4'
      }
    }

    const res = mockResponse()

    await videoController.addVideo(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Cant add any more videos'
    })
  })

  test('Should return error when no req.body.video', async () => {
    const req = {
      params: {
        id: 'b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b2'
      },
      body: {
        video: ''
      }
    }

    const res = mockResponse()

    await videoController.addVideo(req, res)

    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: 'Invalid Video File'
    })
  })
})

describe('POST /deleteVideo behavior', () => {
  let videoId
  beforeAll(async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9'
      },
      body: {
        video:
          'https://res.cloudinary.com/dqvhvly7q/video/upload/v1688334862/profiles/moxogt6rruhib0jmmjhc.mp4'
      }
    }
    const res = mockResponse()

    await videoController.addVideo(req, res)

    videoId = res.json.mock.calls[0][0].data.id

    expect(res.status).toBeCalledWith(201)
    expect(res.json).toBeCalledWith({
      msg: 'Success upload video',
      data: expect.any(Object)
    })
  })
  test('Should return success when succesfully delete video from user', async () => {
    const req = {
      params: {
        id: '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9',
        videoid: videoId
      }
    }
    const res = mockResponse()

    await videoController.deleteVideo(req, res)
    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeCalledWith({
      msg: 'Success'
    })
  })

  test("Should return error when userId not presence", async () => {
    const req = {
        params: {
            id: "",
            videoid: videoId
        }
    }

    const res = mockResponse()

    await videoController.deleteVideo(req, res)
    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: "Bad Request"
    })
  })

  test("Should return error when userId not found", async () => {
    const req = {
        params: {
            id: "b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b1",
            videoid: videoId
        }
    }

    const res = mockResponse()

    await videoController.deleteVideo(req, res)
    expect(res.status).toBeCalledWith(404)
    expect(res.json).toBeCalledWith({
      msg: "User not found"
    })
  })

  test("Should return error when videoId not found", async () => {
    const req = {
        params: {
            id: "b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b2",
            videoid: 999
        }
    }

    const res = mockResponse()

    await videoController.deleteVideo(req, res)
    expect(res.status).toBeCalledWith(404)
    expect(res.json).toBeCalledWith({
      msg: "Video ID not found"
    })
  })

  test("Should return error when videoId not found", async () => {
    const req = {
        params: {
            id: "b0001bb5-56b0-4fdc-920f-f8f4b5ccd7b2",
            videoid: ""
        }
    }

    const res = mockResponse()

    await videoController.deleteVideo(req, res)
    expect(res.status).toBeCalledWith(400)
    expect(res.json).toBeCalledWith({
      msg: "Bad Request"
    })
  })
})
